import dashboardHtml from './dashboard.html';

/* @ngInject */
let dashboardComponent = {
  template: dashboardHtml,
  controllerAs: 'dashboard',
  controller: function(dashboardService) {
    const vm = this;
    vm.title = dashboardService.title();
  }

}
export default dashboardComponent;
