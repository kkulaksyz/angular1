import angular from 'angular';
import routing from './dashboard.route';
import component from './dashboard.component';
import service from './dashboard.service';
/* @ngInject */
angular
  .module('dashboard', [])
  .component('dashboard', component)
  .factory('dashboardService', service)
  .config(routing);
