function dashboardRoutes($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.when('', '/dashboard');
  $urlRouterProvider.when('/', '/dashboard');
  $stateProvider
    .state('dashboard', {
      url: '/dashboard',
      component: 'dashboard'
    });

  $urlRouterProvider.otherwise('/');
}
/* @ngInject */
export default dashboardRoutes;
