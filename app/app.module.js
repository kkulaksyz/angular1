import angular from 'angular';
import uirouter from 'angular-ui-router';
import angularAria from 'angular-aria';
import angularAnimate from 'angular-animate';
import angularMaterial from 'angular-material';
import dashboard from './dashboard/dashboard.module';
import campaigns from './campaigns/campaigns.module';

require('./main.scss');
angular.module('app', [
  uirouter,
  angularMaterial,
  angularAria,
  angularAnimate,
  'dashboard',
  'campaigns'
]);
