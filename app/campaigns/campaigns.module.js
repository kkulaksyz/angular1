import angular from 'angular';
import routing from './campaigns.route';
import component from './campaigns.component';
import service from './campaigns.service';
import campaign from './campaign.module';
/* @ngInject */
angular
  .module('campaigns', [ 'campaign' ])
  .component('campaigns', component)
  .factory('campaignsService', service)
  .config(routing);
