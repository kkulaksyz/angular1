import campaignHtml from './campaign.html';

/* @ngInject */
let campaignComponent = {
  template: campaignHtml,
  controllerAs: 'campaign',
  controller: function(campaignService, $stateParams, $log) {
    const vm = this;

    vm.selectedTab = 0;
    vm.network = '';

    vm.allPosts = campaignService.getPosts();

    vm.selectedPosts = [];

    vm.tabs = [
      {
        title: 'pending'
      },
      {
        title: 'rejected'
      },
      {
        title: 'aprroved'
      }
    ];

    vm.filterPosts = (filterBy) => {
      vm.network = filterBy;
    }
    vm.onTabSelected = () => {
      vm.selectedPosts = [ ...vm.allPosts.media.filter(e => {
        if (vm.selectedTab === 0) {
          return !e.rejected && !e.approved
        };
        return vm.selectedTab === 1 ? e.rejected : e.approved;
      }) ]
    }
    vm.reject = (index) => {
      vm.selectedPosts[index].rejected = true;
      vm.selectedPosts[index].approved = false;
      vm.onTabSelected();
    }
    vm.approve = (index) => {
      vm.selectedPosts[index].approved = true;
      vm.selectedPosts[index].rejected = false;
      vm.onTabSelected();
    }
  }

}
export default campaignComponent;
