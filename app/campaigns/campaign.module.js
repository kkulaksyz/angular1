import angular from 'angular';
import component from './campaign.component';
import service from './campaign.service';
/* @ngInject */
angular
  .module('campaign', [])
  .component('campaign', component)
  .factory('campaignService', service);
