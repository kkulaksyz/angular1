import campaignsHtml from './campaigns.html';

/* @ngInject */
let campaignsComponent = {
  template: campaignsHtml,
  controllerAs: 'campaigns',
  controller: function(campaignsService, $window, $document, $timeout) {
    const vm = this;
    vm.title = campaignsService.title();
    vm.allCampaigns = campaignsService.getCampaigns();
    vm.showCampaigns = vm.allCampaigns.slice(0, 7);
    vm.showMore = () => {
      if (vm.showCampaigns.length < vm.allCampaigns.length) {
        let moreCampaigns = vm.allCampaigns.slice(
          vm.showCampaigns.length, (vm.showCampaigns.length + 8));

        vm.showCampaigns = [...vm.showCampaigns, ...moreCampaigns];

        $timeout(() => {
          $window.scrollTo(0, $document[0].body.scrollHeight);
        }, 100)
      }
    };
    vm.hideCampaign = (index) => {
      vm.showCampaigns.splice(index, 1);
    };
  }

}
export default campaignsComponent;
