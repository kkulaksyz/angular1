function campaignsRoutes($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.when('/campaigns', '/dashboard/campaigns')
  $stateProvider
    .state('dashboard.campaigns', {
      url: '/campaigns',
      component: 'campaigns'
    })
    .state('dashboard.campaigns.campaign', {
      url: '/:id',
      component: 'campaign'
    })
}
/* @ngInject */
export default campaignsRoutes;
